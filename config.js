"use strict";
require('dotenv').config(process.env.NODE_ENV);

module.exports = {
  PORT: process.env.PORT,
  NODE_ENV: process.env.NODE_ENV,
};

